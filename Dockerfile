FROM php:8.1-apache
#Activation du module rewrite
RUN a2enmod rewrite
#maj et suppressoin de klk files unitil
RUN apt-get update \
  && apt-get install -y libzip-dev git wget --no-install-recommends \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN docker-php-ext-install pdo mysqli pdo_mysql zip;

RUN wget https://getcomposer.org/download/2.0.9/composer.phar \
    && mv composer.phar /usr/bin/composer && chmod +x /usr/bin/composer

COPY apache.conf /etc/apache2/sites-enabled/000-default.conf

# COPY . /var/www
WORKDIR /var/www


CMD ["apache2-foreground"]